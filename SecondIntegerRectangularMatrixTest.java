package lab1;

public class SecondIntegerRectangularMatrixTest {

        public static void main(String[] args) {
            int[][] arr1=new int[800][600];
            int[][] arr2=new int[600][500];
            int[][] arr3=new int[800][500];
            long startTime=System.currentTimeMillis();
//        Assign numbers to arr1
            geNumber(arr1);
            System.out.println("List array arr1:");
            listArray(arr1);
//        Assign numbers to arr2
            geNumber(arr2);
            System.out.println("List array arr2:");
            listArray(arr2);
//        multiplies two rectangular matrices
            multiplyCalculator(arr1,arr2,arr3);

            System.out.println("List array arr3:");
            listArray(arr3);

            long endTime=System.currentTimeMillis();

            long timeElapsed=endTime-startTime;
            System.out.println("The total time for the programme is:"+timeElapsed+"Millis");
        }
        //    Method,assign numbers to array
        public static void geNumber(int[][] arr){
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j]=(int)(Math.random()*10);
                }
            }
        }

        //    Method,list array
        public static void listArray(int[][] arr){
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j]+"\t");
                }
                System.out.println();
            }
        }

        //    Method,multiplies two rectangular matrices
        public static void multiplyCalculator(int[][] arr1,int[][] arr2,int[][] arr3){
            for (int i = 0; i < arr2[0].length; i++) {
                for (int j = 0; j < arr1.length; j++) {
                    for (int k = 0; k <arr2.length ; k++) {
                        arr3[j][i]+=arr1[j][k]*arr2[k][i];
                    }

                }
            }
        }


}
