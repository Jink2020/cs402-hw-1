package lab1;

public class DoubleRectangularMatrixTest {

    public static void main(String[] args) {
        double[][] arr1=new double[800][600];
        double[][] arr2=new double[600][500];
        double[][] arr3=new double[800][500];
        long startTime=System.currentTimeMillis();
//        Assign numbers to arr1
        geNumber(arr1);
        System.out.println("List array arr1:");
        listArray(arr1);
//        Assign numbers to arr2
        geNumber(arr2);
        System.out.println("List array arr2:");
        listArray(arr2);
//        multiplies two rectangular matrices
        multiplyCalculator(arr1,arr2,arr3);

        System.out.println("List array arr3:");
        listArray(arr3);

        long endTime=System.currentTimeMillis();

        long timeElapsed=endTime-startTime;
        System.out.println("The total time for the programme is:"+timeElapsed+"Millis");
    }
    //    Method,assign numbers to array
    public static void geNumber(double[][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j]=Math.random()*10;
            }
        }
    }

    //    Method,list array
    public static void listArray(double[][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
    }

    //    Method,multiplies two rectangular matrices
    public static void multiplyCalculator(double[][] arr1,double[][] arr2,double[][] arr3){
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2[0].length; j++) {
                for (int k = 0; k <arr2.length ; k++) {
                    arr3[i][j]+=arr1[i][k]*arr2[k][j];
                }

            }
        }
    }
}
